fn ++ {|n|
  + $n 1
}

fn -- {|n|
  - $n 1
}

fn neg {|n|
  * $n -1
}

fn is-zero {|n|
  == $n 0
}

fn is-neg {|n|
  < $n 0
}

fn is-one {|n|
  == $n 1
}

fn -minmax {|cb @numbers|
  use ./common
  var l = (common:to-list $@numbers)
  if (is-zero (count $l)) {
    return
  }
  var m = $l[0]
  all $l[1..] | each {|n|
    if ($cb $n $m) {
      set m = $n
    }
  }
  put $m
}

fn min {|@numbers|
  -minmax $'<~' $@numbers
}

fn max {|@numbers|
  -minmax $'>~' $@numbers
}
