fn to-list {|@argv|
  var c = (count $argv)
  if (== $c 0) {
    put [ (all) ]
  } elif (== $c 1) {
    put $argv[0]
  } else {
    fail (printf '0 or 1 argument needed given %d arguments: %v' $c $argv)
  }
}

fn cond {|cond v1 v2|
  if $cond {
    put $v1
  } else {
    put $v2
  }
}

fn cexec {|cond v1 v2|
  var r = (cond $cond $v1 $v2)
  if (eq (kind-of $r) fn) {
    $r
  } else {
    put $r
  }
}

fn less {|cmp|
  put {|e1 e2| < ($cmp $e1 $e2) 0}
}
