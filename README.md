# elv-lib

elv-lib includes some useful module functions and completions



## mods

This folder groups modules for different usages:

### file

file provides functions to complete a filename/directory motive with different methods:

* exact motive
* contains motive (with or without deep search)

- search by prefix (with or without deep search)
- search by suffix (with or without deep search)
- filter results by given extension

### format

format provides functions to beautify a given input. Examples:

- format:size returns a size (in bytes) in a human-readable format
- format:list displays data in columns

### ip

ip provides useful functions to manipulate IPv4 or IPv6 addresses.

### list

list provides functions to manipulate data of type list. Examples:

* list:foreach acts as builtin each with the callback function on the form {|i v| } where i is the index in the list an v the value
* list:reach acts as builtin each in reverse order
* list:filter filters in a list with the given callback criterium
* list:contains check if a list contains a specific entry or an entry which passes a callback
* etc.

### map

map provides functions to manipulate data of type map.

### num

num provides useful functions for number operations which are not part of builtin functions or math: module functions.

### option

### pdf

pdf provides functions to manipulate PDF files (qpdf is required to make this module work).

## completion

The folder completion contains the completion for some programs:

- [arc](https://github.com/mholt/archiver)
- [kcp](https://github.com/bvaudour/kcp)
- mpv
- pacman
- ssh
