use re
use ./file

var commands = [
  help
  archive
  unarchive
  extract
  ls
]

var description = [
  &help='Display the help'
  &archive='Create un new archive'
  &unarchive='Extract all'
  &extract='Extract a single file'
  &ls='List the content'
]

var extensions = [ tar bz2 zip gz lz4 sz xz zst rar ]

fn -comp-commands {
  each {|c|
    edit:complex-candidate $c &display=(printf '%s (%s)' $c $description[$c])
  } $commands
}

fn -comp-inline-files {|archive|
  try {
    arc ls $archive | re:awk {|_ @argv| put $argv[-1] }
  } catch e {
    nop
  }
}

fn complete {|@argv|
  var c cmd = (count $argv) $argv[1]
  if (== $c 2) {
    -comp-commands
  } elif (== $c 3) {
    if (not (has-value [help archive] $cmd)) {
      file:complete $argv[-1] $@extensions
    }
  } else {
    if (eq $cmd archive) {
      edit:complete-filename $@argv
    } elif (eq $cmd extract) {
      var archive = $argv[2]
      -comp-inline-files $archive
    }
  }
}

set edit:completion:arg-completer[arc] = $complete~
