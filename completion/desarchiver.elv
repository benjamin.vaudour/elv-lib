use ./file

var extensions = [ tar bz2 zip gz lz4 sz xz zst rar ]

fn complete {|@argv|
  var m = $argv[-1]
  file:complete $m $@extensions
}

set edit:completion:arg-completer[desarchiver] = $complete~
