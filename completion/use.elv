use path
use str
use ./file

var libdir          = $E:HOME/.config/elvish/lib
var builtin_modules = [
  builtin
  epm
  file
  math
  path
  re
  readline-binding
  store
  str
  unix
]

set edit:completion:arg-completer[use] = {|@argv|
  use str
  use path
  all $builtin_modules
  put $libdir/**.elv | each {|f|
    if (path:is-regular $f) {
      str:trim-prefix $f $libdir/
    }
  } | each {|f| str:trim-suffix $f .elv }
  if (> (count $argv) 1) {
    file:complete $argv[-1] elv | each {|f| str:trim-suffix $f .elv }
  }
}
