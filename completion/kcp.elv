fn -remotes-packages { kcp -lN }

var options = [
  -h
  -v
  -i
  -di
  -u
  -l
  -lN
  -lS
  -lI
  -lO
  -lx
  -lxS
  -lxI
  -lxO
  -lf
  -s
  -g
  -V
]

var np = [
  -i
  -di
  -s
  -g
  -V
]

fn complete {|@argv|
  var c = (count $argv)
  if (== $c 2) {
    all $options
  } elif (and (== $c 3) (has-value $np $argv[-2])) {
    -remotes-packages
  }
}

set edit:completion:arg-completer[kcp] = $complete~
