use ./file

var extensions = [
  aac
  ape
  avi
  divx
  flac
  flv
  m3u
  m4a
  m4v
  mp3
  mp4
  mpeg
  mpg
  mkv
  mng
  mov
  qt
  oga
  ogg
  ogm
  ogv
  opus
  ra
  rv
  ts
  vob
  wav
  webm
  wmv
  wma
  wmx
]

fn complete {|@argv|
  var c = (count $argv)
  if (== $c 2) {
    put --speed
    file:complete $argv[-1] $@extensions
  } elif (== $c 3) {
    if (eq $argv[-2] --speed) {
      put 0.8 0.9 1.0 1.1 1.2
    } else {
      file:complete $argv[-1] $@extensions
    }
  } else {
    file:complete $argv[-1] $@extensions
  }
}

set edit:completion:arg-completer[mpv] = $complete~
