use re
use str
use ../mods/file
use ../mods/list

fn -local-packages { pacman -Q | re:awk {|_ p @_| put $p } }

fn -repo-packages {
  var packages = [(pacman -Ss | list:pforeach &step=2 {|_ v|
    put $v
  } | re:awk {|_ p @_|
    put $p
  })]
  var spackages = [&]
  peach {|p|
    str:split '/' $p
  } $packages | peach {|e|
    set spackages[$e] = $nil
  }
  keys $spackages
  all $packages
}

var options = [
  -h
  -V
  -Q
  -Qs
  -Ql
  -Qi
  -Qm
  -Qdt
  -Qo
  -R
  -Rsn
  -S
  -Ss
  -Si
  -Sii
  -Syu
  -Syyu
  -U
  -D
]

var asdeps = [
  -S
  -U
  -D
]

var lpack = [
  -Q
  -Qs
  -Ql
  -Qi
  -D
  -R
  -Rsn
]

var rpack = [
  -S
  -Ss
  -Si
  -Sii
]

var dpack = [
  -U
]

var fpack = [
  -Qo
]

var extensions = [ tar.zst tar.xz tar.gz tar.bz2 ]

fn complete {|@argv|
  var c = (count $argv)
  if (< $c 3) {
    all $options
  } else {
    var cmd = $argv[1]
    if (and (== $c 3) (has-value $asdeps $cmd)) {
      put --asdeps --asexplicit
    }
    if (has-value $lpack $cmd) {
      -local-packages
    } elif (has-value $rpack $cmd) {
      -repo-packages
    } elif (has-value $dpack $cmd) {
      file:match-extensions $argv[-1] $@extensions
    } elif (has-value $fpack $cmd) {
      edit:complete-filename $argv[-1]
    }
  }
}

set edit:completion:arg-completer[pacman] = $complete~
