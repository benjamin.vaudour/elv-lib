var options = [
  z
  l
  x
  b
  g
  h
]

var description = [
  &z='zst compression (default)'
  &l='lz4 compression'
  &x='xz compression'
  &b='bz2 compression'
  &g='gzip compression'
  &h='display help'
]

fn -options {
  each {|o|
    put [&short=$o &desc=$description[$o]]
  } $options
}

fn complete {|@argv|
  edit:complete-getopt $argv [(-options)] [ $edit:complete-filename~ ...]
}

set edit:completion:arg-completer[archiver] = $complete~
