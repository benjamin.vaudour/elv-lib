use ../mods/common
use ../mods/file

fn complete {|motive @extensions|
  var type = (common:cond (eq $motive '') prefix deep-prefix)
  file:match-extensions &type=$type $motive $@extensions
}
