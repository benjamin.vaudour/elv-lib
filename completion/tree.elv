
use builtin;
use str;

set edit:completion:arg-completer[tree] = {|@words|
    fn spaces {|n|
        builtin:repeat $n ' ' | str:join ''
    }
    fn cand {|text desc|
        edit:complex-candidate $text &display=$text' '(spaces (- 14 (wcswidth $text)))$desc
    }
    edit:complete-filename $@words
    var command = 'et'
    for word $words[1..-1] {
        if (str:has-prefix $word '-') {
            break
        }
        set command = $command';'$word
    }
    var completions = [
        &'et'= {
            cand -d 'Print physical or logical file size'
            cand --disk-usage 'Print physical or logical file size'
            cand --time 'Which kind of timestamp to use; modified by default'
            cand -L 'Maximum depth to display'
            cand --level 'Maximum depth to display'
            cand -p 'Regular expression (or glob if ''--glob'' or ''--iglob'' is used) used to match files'
            cand --pattern 'Regular expression (or glob if ''--glob'' or ''--iglob'' is used) used to match files'
            cand -t 'Restrict regex or glob search to a particular file-type'
            cand --file-type 'Restrict regex or glob search to a particular file-type'
            cand -s 'Sort-order to display directory content'
            cand --sort 'Sort-order to display directory content'
            cand -T 'Number of threads to use'
            cand --threads 'Number of threads to use'
            cand -u 'Report disk usage in binary or SI units'
            cand --unit 'Report disk usage in binary or SI units'
            cand --completions 'Print completions for a given shell to stdout'
            cand -C 'Turn on colorization always'
            cand --force-color 'Turn on colorization always'
            cand -f 'Follow symlinks'
            cand --follow 'Follow symlinks'
            cand -F 'Print disk usage information in plain format without the ASCII tree'
            cand --flat 'Print disk usage information in plain format without the ASCII tree'
            cand -H 'Print disk usage in human-readable format'
            cand --human 'Print disk usage in human-readable format'
            cand -i 'Do not respect .gitignore files'
            cand --no-ignore 'Do not respect .gitignore files'
            cand -I 'Display file icons'
            cand --icons 'Display file icons'
            cand -l 'Show extended metadata and attributes'
            cand --long 'Show extended metadata and attributes'
            cand --octal 'Show permissions in numeric octal format instead of symbolic'
            cand --glob 'Enables glob based searching'
            cand --iglob 'Enables case-insensitive glob based searching'
            cand -P 'Remove empty directories from output'
            cand --prune 'Remove empty directories from output'
            cand --dirs-first 'Sort directories above files'
            cand -. 'Show hidden files'
            cand --hidden 'Show hidden files'
            cand --no-git 'Disable traversal of .git directory when traversing hidden files'
            cand --dirs-only 'Only print directories'
            cand --inverted 'Print tree with the root directory at the topmost position'
            cand --no-color 'Print plainly without ANSI escapes'
            cand --no-config 'Don''t read configuration file'
            cand --suppress-size 'Omit disk usage from output'
            cand --truncate 'Truncate output to fit terminal emulator window'
            cand -h 'Print help (see more with ''--help'')'
            cand --help 'Print help (see more with ''--help'')'
            cand -V 'Print version'
            cand --version 'Print version'
        }
    ]
    $completions[$command]
}
