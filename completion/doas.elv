fn complete {|@argv|
  if (and (> (count $argv) 2) (has-key $edit:completion:arg-completer $argv[1])) {
    $edit:completion:arg-completer[$argv[1]] (all $argv[1:])
  } else {
    edit:complete-sudo $@argv
  }
}

set edit:completion:arg-completer[doas] = $edit:complete-sudo~
#edit:completion:arg-completer[doas] = $-complete~
