var dir = $E:HOME/.config/elvish/aliases

for file [(set _ = ?(put $dir/*.elv))] {
  var content = (cat $file | slurp)
  eval $content
}

{
  use ./mods/common
  edit:add-var cond~ $common:cond~
  edit:add-var cexec~ $common:cexec~
}

{
  use ./mods/num
  edit:add-var '++~' $'num:++~'
  edit:add-var '--~' $'num:--~'
  edit:add-var neg~ $num:neg~
  edit:add-var is-neg~ $num:is-neg~
  edit:add-var is-zero~ $num:is-zero~
  edit:add-var is-one~ $num:is-one~
  edit:add-var min~ $num:min~
  edit:add-var max~ $num:max~
}
